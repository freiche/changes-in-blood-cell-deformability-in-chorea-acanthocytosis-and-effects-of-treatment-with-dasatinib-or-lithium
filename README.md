# Changes in blood cell deformability in Chorea-Acanthocytosis and effects of treatment with dasatinib or lithium

This repository contains data files and analysis tools to recreate the plots for Reichel et al. 2021. "Changes in blood 
cell deformability in Chorea-Acanthocytosis and effects of treatment with dasatinib or lithium", 
https://www.frontiersin.org/articles/10.3389/fphys.2022.852946

https://doi.org/10.3389/fphys.2022.852946

The folder `figure_plots` contains Jupyter notebooks that were used to create the figures.

To run the notebooks an installation of python>=3.7 is needed.

The `requirements.txt` and `environment.yml` contain all dependencies for the notebooks to work and can be installed 
either with pip or conda. Please create an virtual environment, e.g. 
with Anaconda's `conda create` command and run the following commands:

Create a conda virtual environment from the environment.yml by running: 
```
conda env create -f environment.yml
```
The default environment name will be `py37_chac`

Or from the requirements.txt:
```
# using Conda
conda create --name <env_name> --file requirements.txt

# using pip
pip install -r requirements.txt
```

For significance testing, a valid installation of R>3.0 is needed and one needs to install the `lme4` package from CRAN 
(`R -e "install.packages('lme4', repos='http://cran.r-project.org')"`). But the analysis is also run through the Jupyter
 notebooks.

The folder `data_preparation` contains notebooks that describe how the raw data was processed to get the data that is 
used to create the figure plots (data under `figure_plots\data`). The raw data is available on figshare:
https://doi.org/10.6084/m9.figshare.c.5793482 .
